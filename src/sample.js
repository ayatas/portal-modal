import React from 'react';
import ReactDOM from 'react-dom';

import Modal from './modal';

class Sample extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalShown: false,
    };
  }
  render() {
    return(
      <div>
        <button onClick={() => { this.setState({ isModalShown: true }) }}>Click to show modal.</button>
        {this.state.isModalShown &&
          <Modal onClose={() => { this.setState({ isModalShown: false }) }} />
        }
      </div>
    );
  }
}

export default Sample;