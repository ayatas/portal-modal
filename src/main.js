import React from 'react';
import ReactDOM from 'react-dom';

import Sample from './sample';

export default () => {
  return(
    <div>
      <h1>Portal Modal</h1>
      <Sample />
    </div>
  );
};